/**
* Created by Work-nekki on 29/10/14.
*/
//function autoSizeBackground() {
//	var height = $( window ).height(),
//		width = $( window ).width(),
//		screenProportions = width/height,
//		imageProportions= 480/278;
//
//	if (screenProportions > imageProportions) {
//		$( ".bg" ).css('background-size', '100% auto');
//	}
//	else {
//		$( ".bg" ).css('background-size', 'auto 100%');
//	}
//}

$(document).ready(function() {

	$('.lazy').lazyLoadXT({
		srcAttr: 'data-src',
		edgeY: 530,
		edgeX: 940
//		blankImage: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',

//		onload: function() {
//			alert('ok');
//		}

//		onload: function() {
//			alert('ok');
//		}
	});
    
   	$('.screen__img').load(function(i,elem) {
		
		var screenImage = $(this);

		var tmpImage = new Image();
		tmpImage.src = screenImage.attr("src");

		var realWidth = tmpImage.width;
		var realHeight = tmpImage.height;
		
		var width = screenImage.actual('width');
		var height = screenImage.actual('height');

		var points = $(this).parent().children('.screen__item');

		points.each(function(i,elem) {
		   
			var point = $(this);

			var curX = point.attr('data-x');
			var curY = point.attr('data-y');
			

			var x = (curX * width) / realWidth;
			var y = (curY * height) / realHeight;
			
			x= x.toFixed(0);
			y= y.toFixed(0);

			point.css('left', x+'px');
			point.css('top', y+'px');
			point.css('display', 'block');
			point.children('i').css('display', 'block');
		});
		
	})
	
//	opacity = 0.08;
//	glow=0;
//	var timer = setInterval(function() {
//		opacity=opacity+0.04;
//		glow++;
//		$(".background-small").css("opacity",opacity.toFixed(2));
//		$(".glow"+glow).css("display",'block');
//		if (opacity > 0.4) {
//			clearInterval(timer);
//
//			$('.light-glow').animate({opacity: 1},50);
//			$('.light-glow').animate({opacity: 0},50);
//
//			$('.background-small, .glow').fadeOut(200);
//			$('.background').fadeIn(1000);
//
//			setTimeout(function() { interiorAnimation();}, 100);
//
//
//
//		}
//	}, 300);
//
//
//	function interiorAnimation() {
//
//		$('.table, .chairL, .palmL, .chairR, .palmR, .cosplay, .toolbar, .header, .poker-room__video').animate(
//			{
//				backgroundSize: '100%',
//				opacity: 1
//			},
//			1000);
//	}
//
//	autoSizeBackground();
//
//	$( window ).resize(function() {
//		autoSizeBackground();
//	});
//
//
//	$(".wrapper__onepage").onepage_scroll({
//		sectionContainer: "section",
//		loop: false,
//		responsiveFallback: 1024
//	});

//	$(".header__play-button").fancybox();

	//Animation initialization

	wow = new WOW(
		{
			boxClass:     'wow',
			animateClass: 'animated',
			offset:       0,
			mobile:    	  false
		}
	)
	wow.init();

	$('.menu_support').owlCarousel({
		navigation: true,
		rewindNav: false,
		items: 2,
		navigationText: false,
		addClassActive: true
	});

//	$('.scroller__item a').click(function() {
//		var stageId = $(this).attr('href');
//		var num = stageId.slice(-1);
//		var stagePosition = $(stageId).position().top;
//		$(this).parents('.scroller').css('top', stagePosition-50*(num-1));
//
//		$(stageId).each(function() {
//			var $content;
//			$content = $(this);
//			return $('html, body').animate({
//				scrollTop: $content.offset().top - 50
//			}, 800);
//		});
//	});

	$('.stage__scroll').click(function() {
		var nextContent = $(this).parent().next();
		return $('html, body').animate({
			scrollTop: nextContent.offset().top
		}, 800);
	});

//	$(document).scroll(function() {
//		$('.stage').each(function() {
//			var positionTop = $(this).scrollTop();
////			if(positionTop == 0) {
////				$(this).css('border', '1px solid red');
////			}
//			console.log();
//		});
//	});

//	$('.tooltip').tipTip({
//		activation: 'click',
//		fadeIn: 800,
//		maxWidth: "auto",
//		edgeOffset: 20,
//		defaultPosition: 'right',
//		keepAlive: true
//	});


	$('.tooltip_stage').tooltipster({
		trigger: "click",
		maxWidth: 300,
		contentAsHTML: true,
		offsetY: 0,
		offsetX: 0,
		position: 'right',

		functionAfter: function() {
			$(this).removeClass('clicked');
		}
	});

	$('.tooltip_form').tooltipster({
//		autoClose: false,
		trigger: "click",
		maxWidth: 200,
		contentAsHTML: true,
		offsetY: -15,
		offsetX: 8,
		position: 'right',

		functionReady: function(){
			$('.tooltipster-base')
				.addClass('tooltip_shadow')
				.append('<i class="tooltip_close"></i>');
		}
	});


	$('.tooltip_close').click(function() {
		$('.tooltip_form').tooltipster('hide');
	});

	$('.tooltip_stage').click(function() {
		$(this).toggleClass('clicked');
	});

	$('.flexslider').flexslider({
		animation: "slide",
		prevText: "",
		nextText: ""
	});

	$('.screen__close').click(function() {
		$(this).parent().hide();
	});

	$('.stage__data').click(function(e) {

		$('.screen__info').each(function() {
			$(this).hide();
		});

		var text = $(this).data('screen');
		text = '#'+text;
		$(text).show();
		return false;


	});

	$(".fancybox").fancybox({
		maxWidth	: 969,
//		maxHeight	: 995,
		fitToView	: false,
		width		: '100%',
		height		: '100%',
		autoSize	: true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

	$('.flexslider').flexslider({
		animation: "slide",
		prevText: "",
		nextText: "",
		controlNav: false,
		directionNav: false
	});


	//content-scroll button

	$('<div id="content-scroll">Вверх</div>').appendTo('body');

	$(window).scroll(function() {
		if ($(this).scrollTop() >= 400) {
			$('#content-scroll').fadeIn();
		}
		else {
			$('#content-scroll').fadeOut();
		};
	});

	$('#content-scroll').click(function() {
		$('html, body').animate( {
			scrollTop: 0
		}, 'slow')
	});

	$('.js-show-text').click(function() {
		var container = $(this).parent();
		$(this)
			.next()
			.show()
			.end()
			.hide()
			.siblings('.stage-text_hide')
			.show();

	});

	$('.stage-text_hide').click(function() {
		$(this)
			.hide()
			.prev()
			.hide()
			.siblings('.js-show-text')
			.show();
	});

	$('.menu_support a').click(function() {
		$(this)
			.addClass('active')
			.parent()
			.siblings()
			.find('a')
			.removeClass('active');
		var sectionId = $(this).attr('href');
		$(sectionId).each(function() {
			$(this)
				.show()
				.siblings()
				.hide();
		});

		if (history.pushState) {
			history.pushState(null, null, sectionId);
		} else {
			location.hash = sectionId;
		}
		return false;
	});

	$('#menu_support').each(function() {
		$(this).css('visibility', 'visible');
	});

});

