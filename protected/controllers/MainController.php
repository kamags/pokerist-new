<?php
class MainController extends BaseController {

	public function actionIndex() {

		$this->render('index');
	}

	public function actionHowToStart() {
	    
		$this->render('howToStart/howToStart');
	}

	public function actionWelcome() {
		$this->render('welcome');
	}

	public function actionChipsAndGold() {
		$this->render('chipsAndGold');
	}

	public function actiongetTheApp() {

		$this->render('getTheApp');
	}
	
	public function actionTest() {
	    $scenario = Scenario::model() ->findAll();
	    


	    
	    $this->render('test', ['scenario' => $scenario]);
	}
	
	public function actionError() {
		$this->layout = 'error';
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				if ($error['type'] == 'PokeristApi_Exception' && $error['errorCode'] == 'invalid_session') {
					Yii::app()->user->logout();
					$this->redirect(Yii::app()->user->loginUrl);
				}
				try {
					$this->render("error{$error['code']}", ['error' => $error]);
				} catch (CException $e) {
					$this->render("error", ['error' => $error]);
				}
			}
		}
	}

	
}
