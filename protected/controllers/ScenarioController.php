<?php


class ScenarioController extends BaseController {

	public function actionIndex() {
	    
		$scenarios =  Scenarios::model()->with('scenario_steps')->findAll([
		    'order'=>'scenario_steps.scenario_id asc, scenario_steps.number asc'
		    ]);
		
	
		//foreach ($ScenarioStep as $value) {
		   // echo $value->title;
		//}

		$this->render('index', [
		    'scenarios' => $scenarios
		]);
	}


	
}
