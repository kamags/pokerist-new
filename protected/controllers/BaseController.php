<?php

abstract class BaseController extends CController {

	public function isIndex() {
		return $this->route == 'main/index';
	}

}
