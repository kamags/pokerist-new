<?php

return [
	'/'                             => 'main/index',
	'how-to-start' 					=> 'scenario/index',
//	'how-to-start2' 			=> 'scenario/index',
//	'how-to-start/chips-and-gold' 	=> 'main/chipsAndGold',
	'get-the-app' 					=> 'main/getTheApp',
	'test' 					=> 'main/test',
//	'contact-us' 					=> 'feedback/contactUs',
];
