<?php

Yii::import('system.collections.CMap');

$config = CMap::mergeArray(require('_main.php'), [
		'components' => [
			'urlManager' => [
				'staticHost' => 's.p.motcom.ru',
			],
			'db'         => [
				'connectionString' => 'mysql:host=127.0.0.1;dbname=pokerist_new',
				'emulatePrepare'   => true,
				'username'         => 'pokerist',
				'password'         => 'H3UcTXGJZzQ9NMy9',
				'charset'          => 'utf8',
			],
			'log'        => [
				'class'  => 'CLogRouter',
				'routes' => [
					[
						'class'  => 'CFileLogRoute',
						'levels' => 'error, warning, info, trace',
					],
//					[
//						'class' => 'CWebLogRoute',
//						'levels' => 'error, warning, info, trace',
//					],
				],
			],
//		'pokeristApi'  => [
//			'class'           => 'application.components.PokeristApiComponent',
//			'playerApiConfig' => [
//				'server'   => 'http://localhost:8080/',
//				'username' => 'larry',
//				'password' => 'acrobat',
//			],
//			'adminApiConfig'  => [
//				'server'   => 'http://localhost:8080/',
//				'username' => 'larry',
//				'password' => 'acrobat',
//			],
//		],
		],
		'params'     => [
			'feedbackNotify'      => ['sergey@larionov.biz' => 'Sergey Larionov'],
			'blogPostsPerPage'    => 5,
			'academyPostsPerPage' => 5,
			'authorPostsPerPage'  => 5,
			'facebookAppId'       => '557233264326610',
		],
	]
);

return $config;
