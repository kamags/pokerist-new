<?php

Yii::import('system.collections.CMap');

$config = CMap::mergeArray(
	require('_main.php'),
	array(
		'components' => [
			'urlManager' => [
				'cacheID'          => null,
				'class'            => 'UrlManager',
				'urlFormat'        => 'path',
				'caseSensitive'    => true,
				'matchValue'       => true,
				'showScriptName'   => false,
				'urlSuffix'        => '/',
				'useStrictParsing' => true,
				'rules'            => require('_routes.php'),
				'staticHost'       => 'new.admin.pokerist.kama.gs',
			],
			'db'                => [
				'connectionString' => 'mysql:host=127.0.0.1;dbname=pokerist_new_db',
				'emulatePrepare'   => true,
				'username'         => 'pokerist_new',
				'password'         => 'Ieheeseikei3',
				'charset'          => 'utf8',
			],
		],
		'params'     => [
			'feedbackNotify' => ['a.lukin@zmeke.com' => 'Anton Lukin'],
		],
	)
);

return $config;
