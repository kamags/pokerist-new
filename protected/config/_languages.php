<?php

return [
	'en'      => ['site' => true, 'title' => 'English', 'originalTitle' => 'English'],
	'ru'      => ['site' => true, 'title' => 'Russian', 'originalTitle' => 'Русский'],
	'it'      => ['site' => false, 'title' => 'Italian', 'originalTitle' => 'Italiano'],
	'el'      => ['site' => false, 'title' => 'Greek', 'originalTitle' => 'Ελληνικά'],
	'ar'      => ['site' => false, 'title' => 'Arabic', 'originalTitle' => 'العربية'],
	'zh_cn'   => ['site' => false, 'title' => 'Simplified Chinese', 'originalTitle' => '简体中文'],
	'fr'      => ['site' => false, 'title' => 'French', 'originalTitle' => 'Français'],
	'de'      => ['site' => false, 'title' => 'German', 'originalTitle' => 'Deutsch'],
	'es'      => ['site' => false, 'title' => 'Spanish', 'originalTitle' => 'Español'],
	'ja'      => ['site' => false, 'title' => 'Japanese', 'originalTitle' => '日本語'],
	'hi'      => ['site' => false, 'title' => 'Hindi', 'originalTitle' => 'Hindi'],
	'zh_hant' => ['site' => false, 'title' => 'Traditional Chinese', 'originalTitle' => '繁體中文'],
];
