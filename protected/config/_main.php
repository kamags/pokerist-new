<?php

return [
	'basePath'    => __DIR__ . DIRECTORY_SEPARATOR . '..',
	'name'        => 'Pokerist.com - an Exciting Mobile and Social Poker Game',

	// preloading 'log' component
	'preload'        => ['log'],

	// autoloading model and component classes
	'import'         => [
		'application.controllers.BaseController',
		'application.models.shared.*',
		'application.models.*',
		'application.components.*',
		'ext.httpclient.*',
		'ext.fs-tools.*',
		'ext.navigation.*',
		'ext.pokerist-auth.*',
		'ext.phpQuery.*',
	],

	'language'       => 'en',
	'sourceLanguage' => 'en',

	'onBeginRequest' => ['LanguageDetector', 'detect'],

	// application components
	'components'     => [
		'urlManager'        => [
			'cacheID'          => null,
			'class'            => 'UrlManager',
			'urlFormat'        => 'path',
			'caseSensitive'    => true,
			'matchValue'       => true,
			'showScriptName'   => false,
			'urlSuffix'        => '/',
			'useStrictParsing' => true,
			'rules'            => require('_routes.php'),
			'staticHost'       => 'new.admin.pokerist.kama.gs',
		],
		'db'                => [
			'connectionString' => 'mysql:host=127.0.0.1;dbname=pokerist_new_db',
			'emulatePrepare'   => true,
			'username'         => 'pokerist_new',
			'password'         => 'Ieheeseikei3',
			'charset'          => 'utf8',
		],
		'errorHandler'      => [
			'errorAction' => 'main/error',
		],
		'log'               => [
			'class'  => 'CLogRouter',
			'routes' => [
				[
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				],
			],
		],
		'viewRenderer'      => [
			'class'         => 'ext.ETwigViewRenderer',
			'twigPathAlias' => 'ext.twig-renderer.lib.Twig',
			'fileExtension' => '.twig',
			'paths'         => [
				'__main__' => 'application.views',
			],
			'functions'     => [
				'count' => 'count',
				'ceil'  => 'ceil',
				'dump'  => 'var_dump',
			],
			'globals'       => [
				'Yii' => 'Yii',
				'Nav' => 'Navigation',
			],
		],
		'mail'              => [
			'class' => 'ext.mail.YiiMail',
		],
		'queue'             => [
			'class' => 'application.components.KamaQueueComponent',
		],
		'pokeristApi'       => [
			'class'           => 'ext.pokerist-api-client.PokeristApiComponent',
			'playerApiConfig' => [
				'server'   => 'http://api.pokerist.kama.gs/',
				'username' => 'pclub_api',
				'password' => 'Queur0wo',
			],
			'adminApiConfig'  => [
				'server'   => 'http://api.pokerist.kama.gs/',
				'username' => 'pclub_admin_api',
				'password' => 'beCh7poo',
			],
		],
		'cache'             => array(
			'class' => 'CFileCache',
		),
//		'authManager'   => array(
//			'class'        => 'application.components.AuthManager',
//			'defaultRoles' => array('guest'),
//		),
		'authManager'       => [
			'class' => 'ext.pokerist-auth.AuthManager',
		],
		'user' => [
			'class'          => 'application.components.WebUser',
			'loginUrl'       => ['main/signIn'],
			'allowAutoLogin' => true,
			'identityCookie' => [
				'path'   => '/',
				'domain' => '.' . (array_key_exists('HTTP_HOST', $_SERVER)? $_SERVER['HTTP_HOST']: null),
			],
			'authTimeout'    => 7776000, // 3 month in seconds
		],
		'player'            => [
			'class' => 'ext.pokerist-auth.PlayerComponent',
		],
		'numberFormatterEx' => [
			'class' => 'application.components.NumberFormatterEx',
		],
		'xsolla'            => [
			'class'    => 'application.components.XsollaComponent',
			'hostname' => 'http://base.pokerist.com',
			'actions'  => [
				'chips' => 'http://base.pokerist.com/poker/automation/buy_chips_actions.php?userID=%s&type=json&clientType=site&special_prices=%d',
				'golds' => 'http://base.pokerist.com/poker/automation/buy_gold_actions.php?userID=%s&type=json&clientType=site&special_prices=%d'
			],
		],
	],

	'params' => [
		'description'         => 'Start playing Pokerist Texas Poker right now. Join our international community and enjoy playing poker 24/7. See you at table!',
		'keywords'            => '',
		'adminEmail'          => 'a.lukin@zmeke.com',
		'supportEmail'        => ['support@pokerist.com' => 'Pokerist Support'],
		'languages'           => require('_languages.php'),
		'senderEmail'         => ['no-reply@kama.gs' => 'Pokerist [no reply]'],
		'partnersEmail'       => 'partnership@kamagames.com',
		'facebookAppId'       => '121943604485916',
		'blogPostsPerPage'    => 10,
		'academyPostsPerPage' => 10,
		'authorPostsPerPage'  => 10,
		'storeAppId'          => [
			'iphone' => 370339723,
		],
		'playGoogleId'        => 'com.kamagames.pokerist',
		'playNowLink'         => 'lppn://playnow',
//		'securityKey'  => 'zBKu4murcW6IRZI2j436ampX2hxfDy6RWY5PjW2o',
	],
];
