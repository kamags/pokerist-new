<?php

Yii::import('system.collections.CMap');

$config = CMap::mergeArray(require('_main.php'), [
		'components' => [
			'urlManager' => [
				'staticHost' => 'new.admin.pokerist.kama.gs',
			],
			'db'                => [
				'connectionString' => 'mysql:host=127.0.0.1:3307;dbname=pokerist_new_db',
				'emulatePrepare'   => true,
				'username'         => 'pokerist_new',
				'password'         => 'Ieheeseikei3',
				'charset'          => 'utf8',
			],
			'log'         => [
				'class'  => 'CLogRouter',
				'routes' => [
					[
						'class'  => 'CFileLogRoute',
						'levels' => 'error, warning, info, trace',
					],
					[
						'class' => 'CWebLogRoute',
						'levels' => 'error, warning, info, trace',
					],
				],
			],
		],
		'params'     => [
			'feedbackNotify'      => ['a.kovalenko@zmeke.com' => 'Alexey Kovalenko'],
			'blogPostsPerPage'    => 10,
			'academyPostsPerPage' => 10,
			'authorPostsPerPage'  => 10,
			'supportEmail' => ['support@pokerist.com' => 'Pokerist Support'],
		],
	]
);

return $config;
