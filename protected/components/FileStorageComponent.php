<?php

class FileStorageComponent extends CComponent {
	/**
	 * Path to storage directory. Allow Yii namespace
	 * @var string
	 */
	protected $path = 'application.runtime';
	protected $pathExpanded;
	protected $readOnly = false;

	public function init() {
		$this->validatePath();
	}

	/**
	 * @param string $content
	 * @param string|null $filename
	 * @param string $directory
	 * @throws FileStorageException
	 * @return string
	 */
	public function putFileContent($content, $filename = null, $directory = '/') {
		$dir = new FSDirectory($this->getPath() . '/' . ltrim($directory, '/'));
		FSDirectory::create($dir->getPath());
		if (!$dir->exists()) {
			throw new FileStorageException("Directory '{$dir}' doesn't exists and can't automatically create it");
		}

		if ($filename === null) {
			$filename = tempnam($dir->getPath(), '');
		}

		$file = new FSFile($this->getFullFilename($filename, $directory));
		if (!$file->isWritable()) {
			throw new FileStorageException("Can't create file {$file->getFilename()}'");
		}

		$file->setContent($content);
		return $filename;
	}

	/**
	 * @param string $filename
	 * @param string $directory
	 * @throws FileStorageException
	 * @throws FileStorageFileNotFoundException
	 * @return string
	 */
	public function putFile($filename, $directory = '/') {
		$file = new FSFile($filename);
		if (!$file->exists()) {
			throw new FileStorageFileNotFoundException("File '{$file->getFilename()}' doesn't exists");
		}
		if (!$file->isReadable()) {
			throw new FileStorageException("File '{$file->getFilename()}' doesn't readable");
		}
		return $this->putFileContent($file->getContent(), basename($filename), $directory);
	}

	/**
	 * @param string $filename
	 * @param string $directory
	 * @throws FileStorageFileNotFoundException
	 * @throws FileStorageFileNotWritableException
	 * @return bool
	 */
	public function removeFile($filename, $directory = '/') {
		$file = new FSFile($this->getFullFilename($filename, $directory));
		if (!$file->exists()) {
			throw new FileStorageFileNotFoundException("File '{$file->getFilename()}' doesn't exists");
		}
		if (!$file->isWritable()) {
			throw new FileStorageFileNotWritableException("File '{$file->getFilename()}' doesn't writable");
		}
		return $file->delete();
	}

	/**
	 * @param string|null $filename
	 * @param string $directory
	 * @throws FileStorageException
	 * @throws FileStorageFileNotFoundException
	 * @return bool|string
	 */
	public function getFileContent($filename = null, $directory = '/') {
		$file = new FSFile($this->getFullFilename($filename, $directory));

		if (!$file->exists()) {
			throw new FileStorageFileNotFoundException("File '{$file->getFilename()}' doesn't exists");
		}

		if (!$file->isReadable()) {
			throw new FileStorageException("File '{$file->getFilename()}' doesn't readable");
		}

		return $file->getContent();
	}

	public function getFullFilename($filename, $directory = '/') {
		if (!FSFile::isAcceptableFilename($filename)) {
			throw new FileStorageException("Filename '{$filename}' doesn't accept");
		}

		$dir = new FSDirectory($this->getPath() . '/' . ltrim($directory, '/'));
		if (!$dir->exists()) {
			FSDirectory::create($dir->getPath());
			if (!$dir->exists()) {
				throw new FileStorageException("Can't create directory '{$dir->getPath()}'");
			}
		}

		return rtrim($dir->getPath(), '/') . '/' . $filename;
	}


	/**
	 * @param string $path
	 */
	public function setPath($path) {
		$this->path         = $path;
		$this->pathExpanded = null;
		$this->validatePath();
	}

	public function getPath() {
		if ($this->pathExpanded === null) {
			$this->pathExpanded = Yii::getPathOfAlias($this->path);
		}
		return $this->pathExpanded;
	}

	protected function validatePath() {
		if (!is_dir($this->getPath())) {
			throw new FileStorageException("Path '{$this->getPath()}' isn't directory");
		}
		if (!$this->readOnly && !is_writable($this->getPath())) {
			throw new FileStoragePathNotWritableException("Path '{$this->getPath()}' isn't writable");
		}
	}
}


class FileStorageException extends CException {

}

class FileStorageFileNotFoundException extends FileStorageException {

}

class FileStorageFileNotWritableException extends FileStorageException {

}

class FileStoragePathNotWritableException extends FileStorageException {

}
