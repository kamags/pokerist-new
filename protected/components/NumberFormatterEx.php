<?php

/**
 * @property string intCurrencyFormat
 */
class NumberFormatterEx extends CApplicationComponent {
	private $_intCurrencyFormats = [];

	public function getIntCurrencyFormat() {
		$locale = Yii::app()->locale;
		if (!array_key_exists($locale->id, $this->_intCurrencyFormats)) {
			$this->_intCurrencyFormats[$locale->id] = str_replace('0.00', '0', $locale->getCurrencyFormat());
		}
		return $this->_intCurrencyFormats[$locale->id];
	}

	public function formatIntCurrency($value, $currency) {
		return Yii::app()->numberFormatter->format($this->getIntCurrencyFormat(), $value, $currency);
	}
}
