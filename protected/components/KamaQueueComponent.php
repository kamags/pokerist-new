<?php

class KamaQueueComponent extends CApplicationComponent {
	/**
	 * @param string $taskName
	 * @param array $params
	 * @param bool $force
	 * @throws KamaQueueException
	 * @return bool
	 */
	public function addTask($taskName, array $params = array(), $force = false) {
		if (!is_array($params)) {
			throw new KamaQueueException('Wrong parameters');
		}

		$task = new QueueTask();
		$task->attributes = array(
			'task' => (string) $taskName,
			'params' => $params,
		);

		if (!$force && $task->isHashExists()) {
			throw new KamaQueueException('Task in queue already exists');
		}

		return $task->save();
	}
}

class KamaQueueException extends CException {

}
