<?php

class LanguageDetector {
	const SESSION_LANGUAGE_VAR = 'LanguageDetector__language';

	public static function detect($event) {
		/** @var $app CApplication */
		$app = $event->sender;

		/** @var CHttpSession $session */
		if (($session = $app->getComponent('session')) !== null && ($language = $session->get(self::SESSION_LANGUAGE_VAR))) {
			$app->language = $language;
		} else {
			$allowedLanguages = array_keys($app->params->itemAt('languages'));

			$preferredLanguage = $app->request->preferredLanguage;
			if (in_array($preferredLanguage, $allowedLanguages)) {
				$app->language = $preferredLanguage;
			}
			list($preferredLanguage) = explode('_', $preferredLanguage, 2);
			if (in_array($preferredLanguage, $allowedLanguages)) {
				$app->language = $preferredLanguage;
			}
		}

		if ($session) {
			$session->add(self::SESSION_LANGUAGE_VAR, $app->language);
		}
	}

	public static function rememberCurrentLanguage() {
		$app = Yii::app();

		/** @var CHttpSession $session */
		if (($session = $app->getComponent('session')) !== null) {
			$session->add(self::SESSION_LANGUAGE_VAR, $app->language);
		}
	}
}
