<?php

class m141118_235803_create_scenario_table extends DbMigration {
	public function safeUp() {
		$this->createTable('scenario', array(
	       'id' => 'pk',
	       'name' => 'VARCHAR(255) NOT NULL',
	       ) , 'ENGINE=InnoDB CHARSET=utf8'); 
	}

	public function safeDown() {
	    
		dropTable('scenario');
		return false;

	}
}
