<?php

class m141119_001059_add_scenario_content extends DbMigration {
	public function safeUp() {
	    
	        $this->insert('scenario', array('name' => 'scenario_001'));
		$this->insert('scenario', array('name' => 'scenario_002'));

	}

	public function safeDown() {
	    
		$this->getDbConnection()->createCommand('DELETE FROM scenario WHERE name = "scenario_001"')->execute();
		$this->getDbConnection()->createCommand('DELETE FROM scenario WHERE name = "scenario_002"')->execute();
 

	}
}
